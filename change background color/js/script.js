'use strict';

const initChangeColor = () => {
  const button = document.getElementById('button');

  button && document.getElementById('button').addEventListener("click", () => {
    const R = Math.floor(Math.random() * 256);
    const G = Math.floor(Math.random() * 256);
    const B = Math.floor(Math.random() * 256);
    const rgbColor = 'RGB(' + R + ', ' + G + ', ' + B + ')';

    document.getElementById('background').style.background = rgbColor;
    document.getElementById('text').innerHTML = `${rgbColor}`;
  });
}

document.addEventListener("DOMContentLoaded", initChangeColor);
