'use strict';

import menuMockupDataJson from './data.json' assert {type: 'json'};

const initTabs = () => {
    const tabs = document.querySelectorAll('.btn');
    let parent = document.getElementById('menu');

    const renderItem = (item) => {
        const template = `
          <div class="menu_item ${item.type}">
            <img src="${item.img}" alt="${item.name}">
            <div class="item_title">
              <h3>${item.name}</h3>
              <p class="price">${item.price}</p>
            </div>
            <p class="description">${item.description}</p>
          </div>
        `;

        return template;
    };

    menuMockupDataJson.map((item) => {
      parent.insertAdjacentHTML('beforeend', renderItem(item));
    });

    tabs.forEach(function(item) {
        item.addEventListener('click', () => {
            let currentBtn = item;

            tabs.forEach(function(item) {
                item.classList.remove('active');
            });

            currentBtn.classList.add('active');

            let text = currentBtn.getAttribute('data-food-type-btn');

            const createAllItems = () => {
                menuMockupDataJson.map((item) => {
                  parent.insertAdjacentHTML('beforeend', renderItem(item));
                });
            };

            const createItem = (text) => {
                menuMockupDataJson
                  .filter(item => item.type === text)
                  .map((item) => {
                    parent.insertAdjacentHTML('beforeend', renderItem(item));
                  });
            };

            parent.replaceChildren();

            if (text === 'all'){
                createAllItems();
            } else {
                createItem(text);
            };
        });
    });
};
document.addEventListener("DOMContentLoaded", initTabs);
